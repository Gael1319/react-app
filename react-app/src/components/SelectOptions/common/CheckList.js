import React, { Component } from 'react'
import styled from 'styled-components'
import Options from './Options.js'
import options from '../constants/options.js'

const Card = styled.div`
  height:150px;
  width: 350px;
  border-radius: 24px;
  background-color: #21d0d0;
  padding:12px;
  opacity:0.8;
  display: flex;
  align-items: center;
`
function CardLayout(props) {
  const content = props.options.map((option) =>
    <div key={option.id}>
      <Options option={option}>

      </Options>
    </div>
  );
  return (
    <div>
      {content}
    </div>
  );
}

const App = () => {
  return (
    <Card>
      <CardLayout options={options}/>
    </Card>
  );
}

export default App;
