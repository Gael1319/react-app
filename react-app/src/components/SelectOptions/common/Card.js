import React, { Component } from 'react'
import styled from 'styled-components'

const Card = styled.div`
  height:150px;
  width: 350px;
  border-radius: 5px;
  background-color: #fff0ea;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-left: 24px;
  padding-right: 24px;
`

const Title = styled.div`
  width: 300px;
  color: #f06f37;
  font-size: 16px;
  font-weight: bold;
  text-transform: uppercase;
  text-align:center;
  padding-bottom: 12px
`

const Button = styled.button`
  padding: 8px 25px 8px 25px;
  background-color: #f06f37;
  text-transform: uppercase;
  color: white;
  font-size: 14px;
  font-weight: bold;
  border: 1px solid  #f06f37;
  border-radius: 16px;
  opacity: 0.8;
  cursor: pointer
`
class CardText extends Component {
  render() {
    return (
      <Card>
        <Title>
          Use code, Pigi100 for RS_100 OFF on your first order
        </Title>
        <Button>
          Grab Now
        </Button>
      </Card>
    )
  }
}

export default CardText

