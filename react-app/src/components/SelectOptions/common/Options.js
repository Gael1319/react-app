import styled from 'styled-components'
import React, { useState, useEffect } from 'react'

const ContentCard = styled.div`
  display: flex;
  justify-content: space-between;
  padding:4px;
  align-items: center;

`
const Title = styled.p`
  width: 300px;
  color: white;
  font-weight:bold;
  font-size: 14px;
  text-transform: uppercase;
  text-align:left;
`

const Circle = styled.div`
  border-radius:50%;
  border: 2px solid white;
  height: 35px;
  width: 35px;
  cursor: pointer;
  background-color: ${props => props.visible ? 'white': 'transform'}
`

const App = (option) => {
  const state = option.state
  const [visible, setVisible] = useState(false)
  useEffect(() => {
    setVisible(state)
  }, [state])

  const handleChange = () => {
    setVisible(!visible)
  }
  return (
    <ContentCard>
       <Title>
        {option.option.title}
       </Title>
       <Circle visible={visible} onClick={handleChange}></Circle>
    </ContentCard>
  );
}

export default App;
