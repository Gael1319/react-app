const options = [
  {
    id: 1,
    title: 'Price low to high',
    state: false
  },
  {
    id: 2,
    title: 'Price low to low',
    state: false
  },
  {
    id: 3,
    title: 'Popularity',
    state: true
  },
]

export default options
