import styled from 'styled-components'
import Icon from './Icon.js';
import CardText from './CardText.js'
import options from '../constants/options.js'

const ContentCard = styled.div`
  display: flex;
  justify-content: center;
`
const Card = styled.div`
  height:100px;
  width: 400px;
  background-color: aliceblue
`

const Container = styled.div`
  height:100%;
  display:grid;
  grid-template-columns: 100px auto;
`
function CardLayout(props) {
  const content = props.options.map((option) =>
    <ContentCard key={option.id}>
      <Card>
        <Container>
          <Icon option={option}/>
          <CardText option={option}/>
        </Container>
      </Card>
    </ContentCard>
  );
  return (
    <div>
      {content}
    </div>
  );
}


const App = () => {
  return (
    <CardLayout options={options}/>
  );
}

export default App;
