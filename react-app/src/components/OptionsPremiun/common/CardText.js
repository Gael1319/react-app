import React, { Component } from 'react'
import styled from 'styled-components'

const Container = styled.div`
  padding:20px;
  text-align:left
`

const Title = styled.div`
  font-size: 16px;
  font-weight: bold;
`

const Chip = styled.span`
  margin-left: 8px;
  background-color: #ECEFF1;
  color: #90A4AE;
  border-radius: 8px;
  font-size: 12px;
`

const ContentChip = styled.span`
  padding: 8px;
`

const Subtitle = styled.div`
  font-size: 14px;
`

const Action = styled.div`
  font-size: 14px;
  font-weight: bold;
  color: blue;
`
class CardText extends Component {
  render() {
    const prop = this.props
    return (
      <Container>
        <Title>
          <span>
            {prop.option.title}
          </span>
          <Chip>
            <ContentChip>
            { prop.option.active ? 'Activado' : 'Desactivado' }
            </ContentChip>
          </Chip>
        </Title>
        <Subtitle>
          {prop.option.content}
        </Subtitle>
        <Action>
          {prop.option.action}
        </Action>
      </Container>
    )
  }
}

export default CardText
