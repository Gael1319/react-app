import styled from 'styled-components'
import React, { Component } from 'react'


const Container = styled.div`
  margin:auto;
`
const ContentImage = styled.div`
  width: 80px;
  height:80px;
  background-color: ${props => props.color || "palevioletred"};;
  display: flex;
  align-items: center;
  justify-content: center;
`
const Image = styled.img`
  width: 30px;
  height: 30px;
`
class Icon extends Component {
  render() {
    const prop = this.props
    console.log(prop.option.color)
    return (
      <Container>
        <ContentImage color={prop.option.color}>
          <Image/>
        </ContentImage>
      </Container>
    )
  }
}

export default Icon
