const options = [
  {
    id: 1,
    title: 'Paquete Premiun',
    content: 'Descrubre nuevas funciones',
    action: 'Hazte Premium',
    color: '#fff6e5',
    active: false
  },
  {
    id: 2,
    title: 'Dominio',
    content: 'Registra tu propio dominio, p, ej: www.jwit.ar',
    action: 'Registrar un nuevo dominio',
    color: '#ebfcf1',
    active: false
  },
  {
    id: 3,
    title: 'Cuentas de email',
    content: 'Envía correos profesionales',
    action: 'Crear nueva cuenta de email',
    color: '#f4f2ff',
    active: false
  },
]

export default options
