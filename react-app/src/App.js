//import Card from './components/OptionsPremiun/common/Card.js'
import './App.css';
import Card2 from './components/SelectOptions/common/Card.js'
import CheckList from './components/SelectOptions/common/CheckList.js'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  padding-top: 20px;
  gap:20px
`

const App = () => {
  return (
    <Container>
      <Card2/>
      <CheckList/>
    </Container>
  );
}

export default App;
